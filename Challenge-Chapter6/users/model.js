const md5 = require("md5");
const database = require('../db/models');
const { Op } = require("sequelize");
// const dataUserList = [];

class UserListModel {

    //mendapatkan semua data user, 
    //vanilla query=> SELECT * From "Users"
    getAllDataUser = async () => {
        const dataUserList = await database.User.findAll({
            include: [database.UserBiodata, database.UserHistory]
        });
        return dataUserList;
    };

    //mendapatkan biodata berdasarkan user_id
    biodataByUserId = async (idBio) => {
        return await database.UserBiodata.findOne({
            include: [database.User],
            where: { user_id: idBio }
        });
    }

    //update biodata  berdasarkan user_id
    updateBioByUserId = async (idBio, upsertBio) => {
        return await database.UserBiodata.upsert(
            {
                fullname: upsertBio.fullname,
                address: upsertBio.address,
                phone: upsertBio.phone,
                age: upsertBio.age,
                birth: upsertBio.birth,
                user_id: idBio
            },
            { where: { user_id: idBio } }
        );
    };

    //method menambah data baru ke dalam database table Users
    //vanilla query=> INSERT into table "Users" (username, email, password) value("..","..","..");
    addNewData = async (username, email, password) => {
        return await database.User.create({
            username: username,
            email: email,
            password: md5(password),
        });
    };

    //metode mengecek apakah data sudah teregistrasi atau belum
    //vanilla query=> SELECT * FROM "Users" Where username="..." OR email="...";
    isDataAvailable = async (username, email) => {
        const availableData = await database.User.findOne({
            where: {
                [Op.or]: [
                    { username: username },
                    { email: email }
                ]
            }
        });

        if (availableData) {
            return true;
        } else {
            return false;
        };
    };

    //method verifikasi login
    //vanilla query=> SELECT * FROM "Users" WHERE email="..." AND password = "...";
    verifySignIn = async (email, password) => {
        const dataUser = await database.User.findOne({
            where: {
                [Op.and]: [
                    { email: email },
                    { password: md5(password) }
                ]
            }
        });

        return dataUser;
    };

    recordGameHistories = async (inputGame) => {
        return await database.UserHistory.create({
            playerChoice: inputGame.playerChoice,
            comChoice: inputGame.comChoice,
            gameResult: inputGame.gameResult,
            user_id: inputGame.user_id
        })
    };

    gameHistoryById = async (idGame) => {
        return await database.UserHistory.findAll({
            include: [database.User],
            where: { user_id: idGame }
        });
    }
};

module.exports = new UserListModel();
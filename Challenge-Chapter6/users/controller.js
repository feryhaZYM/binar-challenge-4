const userListModel = require('./model');

class UserListController {

    getAllDataUser = async (req, res) => {
        const allDataUsers = await userListModel.getAllDataUser();
        return res.json(allDataUsers);
    };

    getBiodata = async (req, res) => {
        const { idBio } = req.params;
        try {
            const biodata = await userListModel.biodataByUserId(idBio);

            if (biodata) {
                return res.json(biodata);
            } else {
                res.statusCode = 404;
                return res.json({ message: "id : " + idBio + " not found!" })
            }

        } catch (error) {
            res.statusCode = 400;
            return res.json({ message: "error!" })
        }
    };

    updateBiodata = async (req, res) => {
        const { idBio } = req.params;
        const upsertBio = req.body;
        const updateBio = await userListModel.updateBioByUserId(idBio, upsertBio);
        if (updateBio) {
            res.statusCode = 200;
            return res.json(updateBio);
        }
    }
    registerDataUser = async (req, res) => {
        const { username, email, password } = req.body;
        //validasi input data
        if ((username === undefined || username === "") || (email === undefined || email === "")) {
            res.statusCode = 400;
            return res.json({ message: 'Username or Email is invalid, please entry data correctly!' });
        } else if (password === undefined || password === "") {
            res.statusCode = 400;
            return res.json({ message: 'password is invalid, please entry data correctly!' });
        };

        //cek registrasi
        const availableData = await userListModel.isDataAvailable(username, email);
        if (availableData) {
            res.statusCode = 400;
            return res.json({ message: 'Username or Email already registered!' })
        };
        //menambah data ke dalam database user table
        const newData = userListModel.addNewData(username, email, password);
        if (newData) {
            res.statusCode = 200;
            return res.json({ message: 'New user is added' });
        };
    };

    loginDataUser = async (req, res) => {
        const { email, password } = req.body;
        const dataVerify = await userListModel.verifySignIn(email, password);

        if (dataVerify) {
            return res.json(dataVerify);
        } else {
            res.statusCode = 404;
            return res.json({ message: 'Email or Password is invalid, cannot found user!' })
        };
    };

    gameHistories = async (req, res) => {
        const inputGame = req.body;
        const histories = await userListModel.recordGameHistories(inputGame);
        res.json(histories);
    };

    getGameHistories = async (req, res) => {
        const { idGame } = req.params;
        try {
            const history = await userListModel.gameHistoryById(idGame);

            if (history) {
                return res.json(history);
            } else {
                res.statusCode = 404;
                return res.json({ message: "cannot found game history with id : " + idGame })
            }

        } catch (error) {
            res.statusCode = 400;
            return res.json({ message: "error!" })
        }
    }
};

module.exports = new UserListController();
const express = require("express");
const userRouter = express.Router();
const userListController = require("./controller")


//API to get all data user
userRouter.get("/data-user", userListController.getAllDataUser);

//API Registrasi/sign-up
userRouter.post("/sign-up", userListController.registerDataUser);

//API Login/sign-in
userRouter.post("/sign-in", userListController.loginDataUser);

//API get biodata by id
userRouter.get("/data-user/:idBio", userListController.getBiodata);

//API update biodata by id
userRouter.put("/data-user/:idBio", userListController.updateBiodata);

//API record game history
userRouter.put("/game/history", userListController.gameHistories);

//API get game history
userRouter.get("/game/history/:idGame", userListController.getGameHistories);


module.exports = userRouter;
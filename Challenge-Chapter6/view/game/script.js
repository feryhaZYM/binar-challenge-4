const playerRockButton = document.getElementById("rock");
const playerPaperButton = document.getElementById("paper");
const playerScissorButton = document.getElementById("scissor");
const resetButton = document.getElementById("reset");
const versusMatch = document.getElementById("versus")
const resultWin = document.getElementById("playerWin")
const resultLose = document.getElementById("computerWin")
const resultDraw = document.getElementById("draw")

let pilihanPlayer = "";
let pilihanComputer = "";  

class Komputer {
    comMemilih = () => {
    const pilihanAvailable = ["rockCom", "paperCom", "scissorCom"];
    const rndInt = Math.floor(Math.random() * 3);
    const pilihanCom = pilihanAvailable[rndInt];
    pilihanComputer = pilihanAvailable[rndInt];
    pilihanComputer = pilihanCom;
    this.backgroundCom(pilihanCom);
    // console.log(pilihanComputer);
    matchResults();
    };

    backgroundCom = (pilih) => {
    const dipilihCom = document.getElementById (pilih);
    dipilihCom.style.backgroundColor = "#C4C4C4";
    dipilihCom.style.borderRadius = "30px";
    };
}

const computer = new Komputer ();


// class Player {
//     constructor(playerRockButton, playerPaperButton, playerScissorButton){
//         this.rock = playerRockButton;
//         this.paper = playerPaperButton;
//         this.scissor = playerScissorButton;
//     }
    

//     playerButton () {
//         this.rock.onclick = () => {
//             pilihanPlayer = 'rock';
//             pilihanPlayer = playerButton;
//             this.backgroundPlayer(playerButton);
//             this.disableClick();
//             computer.comMemilih();
//             };
//         this.paper.onclick = () => {
//             pilihanPlayer = 'paper';
//             pilihanPlayer = playerButton;
//             this.backgroundPlayer(playerButton);
//             this.disableClick();
//             computer.comMemilih();
//         }
//         this.scissor.onclick = () => {
//             pilihanPlayer = 'scissor';
//             pilihanPlayer = playerButton;
//             this.backgroundPlayer(playerButton);
//             this.disableClick();
//             computer.comMemilih();
//         }
//     };

//     disableClick = () => {
//         playerRockButton.style.pointerEvents = "none";
//         playerPaperButton.style.pointerEvents = "none";
//         playerScissorButton.style.pointerEvents = "none";
//     };

//     backgroundPlayer = (pilihan) => {
//         pilihan.style.backgroundColor = "#C4C4C4";
//         pilihan.style.borderRadius = "30px";
//         };
// };

// const player = new Player;


const backgroundCom = (pilih) => {
    const dipilihCom = document.getElementById (pilih);
    dipilihCom.style.backgroundColor = "#C4C4C4";
    dipilihCom.style.borderRadius = "30px";
};
const comMemilih = () => {
    const pilihanAvailable = ["rockCom", "paperCom", "scissorCom"];
    const rndInt = Math.floor(Math.random() * 3);
    const pilihanCom = pilihanAvailable[rndInt];
    pilihanComputer = pilihanAvailable[rndInt];
    pilihanComputer = pilihanCom;
    backgroundCom(pilihanCom);
    // console.log(pilihanComputer);
    matchResults();
};



const disableClick = () => {
    playerRockButton.style.pointerEvents = "none";
    playerPaperButton.style.pointerEvents = "none";
    playerScissorButton.style.pointerEvents = "none";
};

playerRockButton.onclick = () => {
    playerRockButton.style.backgroundColor = "#C4C4C4";
    playerRockButton.style.borderRadius = "30px";
    pilihanPlayer = "rock";
    computer.comMemilih();
    disableClick();
    // console.log(pilihanPlayer);
};
playerPaperButton.onclick = () => {
    playerPaperButton.style.backgroundColor = "#C4C4C4";
    playerPaperButton.style.borderRadius = "30px";
    pilihanPlayer = "paper";
    computer.comMemilih();
    disableClick();
    // console.log(pilihanPlayer);
};
playerScissorButton.onclick = () => {
    playerScissorButton.style.backgroundColor = "#C4C4C4";
    playerScissorButton.style.borderRadius = "30px";
    pilihanPlayer = "scissor";
    computer.comMemilih();
    disableClick();
    // console.log(pilihanPlayer);
};


// const matchResults = () => {
//     if(pilihanPlayer === "rock" && pilihanComputer === "rockCom"){
//         console.log("DRAW");
//     }else if(pilihanPlayer === "paper" && pilihanComputer === "paperCom"){
//         console.log("DRAW");
//     }else if(pilihanPlayer === "scissor" && pilihanComputer === "scissorCom"){
//         console.log("DRAW");
//     }else if(pilihanPlayer === "rock" && pilihanComputer === "scissorCom"){
//         console.log("PLAYER 1 WIN");
//     }else if( pilihanPlayer === "paper" && pilihanComputer === "rockCom"){
//         console.log("PLAYER 1 WIN")
//     }else if(pilihanPlayer === "scissor" && pilihanComputer === "paperCom"){
//         console.log("PLAYER 1 WIN")
//     }else if(pilihanPlayer === "rock" && pilihanComputer === "paperCom"){
//         console.log("COM WIN");
//     }else if(pilihanPlayer === "paper" && pilihanComputer === "scissorCom"){
//         console.log("COM WIN")
//     }else if(pilihanPlayer === "scissor" && pilihanComputer === "rockCom"){
//         console.log("COM WIN")
//     }
// }

const matchResults = () => {
    if((pilihanPlayer === "rock" && pilihanComputer === "rockCom")||(pilihanPlayer === "paper" && pilihanComputer === "paperCom")||(pilihanPlayer === "scissor" && pilihanComputer === "scissorCom")){
        console.log("DRAW");
        resultDraw.style.display = "flex";
        versusMatch.style.display = "none"
    }else if((pilihanPlayer === "rock" && pilihanComputer === "scissorCom")||( pilihanPlayer === "paper" && pilihanComputer === "rockCom")||(pilihanPlayer === "scissor" && pilihanComputer === "paperCom")){
        console.log("PLAYER 1 WIN");
        resultWin.style.display = "flex";
        versusMatch.style.display = "none"
    }else if((pilihanPlayer === "rock" && pilihanComputer === "paperCom")||(pilihanPlayer === "paper" && pilihanComputer === "scissorCom")||(pilihanPlayer === "scissor" && pilihanComputer === "rockCom")){
        console.log("COM WIN");
        resultLose.style.display = "flex";
        versusMatch.style.display = "none";
    }
}

resetButton.onclick = () => {
    versusMatch.style.display = "";
    resultDraw.style.display ="none";
    resultWin.style.display ="none";
    resultLose.style.display ="none";
    rock.style.pointerEvents = "";
    paper.style.pointerEvents = "";
    scissor.style.pointerEvents = "";
    rock.style.backgroundColor = "transparent";
    paper.style.backgroundColor = "transparent";
    scissor.style.backgroundColor = "transparent";
    rockCom.style.backgroundColor = "transparent";
    paperCom.style.backgroundColor = "transparent";
    scissorCom.style.backgroundColor = "transparent";
};